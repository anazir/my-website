call plug#begin()

Plug 'mileszs/ack.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'gruvbox-community/gruvbox'
Plug 'Yggdroot/indentLine'
Plug 'AndrewRadev/linediff.vim'
Plug 'godlygeek/tabular'
Plug 'wellle/targets.vim'
Plug 'FooSoft/vim-argwrap'
Plug 'tpope/vim-capslock'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-dadbod'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'
Plug 'sheerun/vim-polyglot'
Plug 'alvan/vim-closetag'
Plug 'junegunn/fzf.vim'
Plug 'ayu-theme/ayu-vim'
Plug 'dbeniamine/cheat.sh-vim'
Plug 'haya14busa/vim-migemo'
Plug 'joequery/Stupid-EasyMotion'
Plug 'lilydjwg/fcitx.vim'
Plug 'tpope/vim-abolish'
Plug 'kyazdani42/nvim-tree.lua'
Plug 'jalvesaq/Nvim-R'
Plug 'yaegassy/coc-intelephense', {'do': 'yarn install --frozen-lockfile'}

call plug#end()

scriptencoding utf-8

set nowrap

if has('gui_running')
  autocmd GUIEnter  * set vb t_vb=    " stop annoying beeping for non-error errors
endif

if exists('&belloff')
  set belloff=all                     " never ring the bell for any reason
endif

set cursorline                        " highlight current line

if has('folding')
  if has('windows')
    set fillchars=vert:┃              " BOX DRAWINGS HEAVY VERTICAL (U+2503, UTF-8: E2 94 83)
  endif
  set foldmethod=indent               " not as cool as syntax, but faster
  set foldlevelstart=99               " start folded
endif

set guioptions-=e                     " only show tabline when more than 1 tab in term style
set guioptions-=T                     " don't show toolbar
set guioptions-=m                     " don't show menu bar
set guioptions-=r                     " don't show right scroll bar
set guioptions-=L                     " don't show left scroll bar

set laststatus=2                      " always show status line
set lazyredraw                        " don't bother updating screen during macro playback

set list                              " show whitespace
set listchars=nbsp:%                  " non breakable characters
set listchars+=tab:>·
set listchars+=extends:»
set listchars+=precedes:«
set listchars+=trail:•

set scrolloff=3                       " start scrolling 3 lines before edge of viewport
set sidescrolloff=3                   " same as scrolloff, but for columns
set shiftround                        " always indent by multiple of shiftwidth
set shortmess+=A                      " ignore annoying swapfile messages
set shortmess+=I                      " no splash screen

set switchbuf=usetab                  " try to reuse windows/tabs when switching buffers

syntax enable
colorscheme gruvbox                   " set colorscheme
set background=dark                   " set background

set number                            " show line number on the line the cursor is on
set relativenumber                    " show relative numbers
set ruler                             " show line/column info at bottom of buffer

set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set autoindent
filetype plugin indent on

set inccommand=split                  " show substitute command in split

" ack.vim --- {{{

" Use ripgrep for searching ⚡️
" Options include:
" --vimgrep -> Needed to parse the rg response properly for ack.vim
" --type-not sql -> Avoid huge sql file dumps as it slows down the search
" --smart-case -> Search case insensitive if all lowercase pattern, Search case sensitively otherwise
let g:ackprg = 'rg --vimgrep --type-not sql --smart-case'

" Auto close the Quickfix list after pressing '<enter>' on a list item
let g:ack_autoclose = 1

" Any empty ack search will search for the work the cursor is on
let g:ack_use_cword_for_empty_search = 1

" Don't jump to first match
cnoreabbrev Ack Ack!

"  __  __                   _
" |  \/  |                 (_)
" | \  / | __ _ _ __  _ __  _ _ __   __ _ ___ 
" | |\/| |/ _` | '_ \| '_ \| | '_ \ / _` / __|
" | |  | | (_| | |_) | |_) | | | | | (_| \__ \
" |_|  |_|\__,_| .__/| .__/|_|_| |_|\__, |___/
"              | |   | |             __/ |
"              |_|   |_|            |___/

" Moving around splits
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
nnoremap <C-h> <C-w>h

" easy substitution for multiple occurences of a word
nnoremap c* *Ncgn
nnoremap cg* g*Ncgn

" replay previous macro if in a normal buffer
nnoremap <expr> <CR> empty(&buftype) ? '@@' : '<CR>'

" map leader to space
let mapleader = "\<Space>"

" Enable noexpandtab
nnoremap <leader>tab :set noexpandtab<CR>
" Reload vimrc
nnoremap <leader>so :source $MYVIMRC<CR>
" Open vimrc
nnoremap <leader>vc :edit ~/vim-config/vimrc<CR>
" <Leader><Leader> -- replace next <++>
nnoremap <nowait><buffer> <Leader><Leader> h/<++><CR>:nohlsearch<CR>cgn
" <Leader>e -- edit file, starting in same directory as current file
nnoremap <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>
" HTML snippet
nnoremap <leader>html :-1read ~/vim-config/.skeleton.html<CR>5j3wa
" Copy the whole file to clipboard
nnoremap <Leader>co mmggVG"+y`m
" Copy the whole file
nnoremap <Leader>ca mmggyG`m
" Open quickfix list
nnoremap <Leader>li :copen<CR>
" Open git status
nnoremap <Leader>gs :Gstatus<CR>
" Git Add evetything
nnoremap <Leader>ga :Git add .<CR>
" Git commit message
nnoremap <Leader>gc :Git commit -m ""<Left>
" Git log
nnoremap <Leader>gl :Git log<CR>
" Git checkout
nnoremap <Leader>gb :Git checkout 
" Unhilight search
nnoremap <Leader>hl :nohlsearch<CR>
" Move Visually selected lines
vnoremap <Leader>j :'<,'>move '>+1<CR>gv=gv
vnoremap <Leader>k :'<,'>move '<-2<CR>gv=gv

nnoremap <Leader>o :only<CR>
nnoremap <Leader>q :quit<CR>
nnoremap <Leader>w :write<CR>

" When pressing Ctr + Win neovim would register it as " so this prevents it
noremap <C-"> <Nop>
" Remap _ to do what - was supposed to do before vim-vinegar
nnoremap _ k^

" Add missing imports. When material-ui for react displays an error with some
" material-ui components missing, you can copy the line with those components
" and this mapping will insert them at the top where your imports are.
" Example :
"  Line 77:8:  'TextField' is not defined  react/jsx-no-undef
"  Line 78:8:  'Drawer' is not defined     react/jsx-no-undef
" Copy these 2 lines then in vim press <leader>ai anywhere and they should be
" inserted.
" Beware this mapping stores the initial position in the mark 'a and then saves
" the buffer after changes.
nnoremap <Leader>ai magg}O<Esc>"+p<C-v>}kf'cimport <Esc>V}k:norm 0f'Da from '@material-ui/core/';<esc>gv:norm 0wyw2t'px<CR>'a:w<CR>

" Duplicate property for react. eg. turn disabled to disabled={ disabled }
nnoremap <leader>dp yiwea={ <esc>pa }<esc>
" Complete useState hook
nnoremap <leader>ch yiwea, set<esc>pbftl~A] = useState();<esc>Iconst [<esc>f(a
" Complete import
nnoremap <leader>ci yiwIimport <esc>A from '';<esc>2hp
" Complete funtion
nnoremap <leader>cf Iconst <esc>A = () => {<esc>o<esc>i};<esc>o<esc>2k0f(a
" Complete className
nnoremap <leader>cc ciwclassName={ classes.<esc>pa }<esc>
" Change className to classes
nnoremap <leader>rc 0/className<CR>cwclasses<esc>f{i{<esc>f}a}<esc>F.bi: <esc>:nohlsearch<CR>F:i
" Complete console.log
nnoremap <leader>cl ciWconsole.log()<esc>hp$
nnoremap <leader>cj ciWconsole.log(JSON.stringify())<esc>2hp$
" Complete coalesce
nnoremap <leader>cs gUUA = COALESCE(, )<esc>3Byef,lpF(a

" Maps <leader>/ so we're ready to type the search keyword
nnoremap <Leader>/ :Ack!<Space>

" Navigate quickfix list with ease
nnoremap <silent> ( :cprevious<CR>
nnoremap <silent> ) :cnext<CR>

" Copy to clipboard
nnoremap <leader>y "+y
" Copy from clipboard
nnoremap <leader>p o<esc>"+p
nnoremap <leader>P O<esc>"+p

" Get all highlight groups
nnoremap <Leader>shg :so $VIMRUNTIME/syntax/hitest.vim<CR>

" Assembly comment
nnoremap <leader>ac :.g/#.*/norm f#D/<CR>:nohlsearch<CR>30A <esc>d32\|a# 

" COC
nmap <silent> [e <Plug>(coc-diagnostic-prev)
nmap <silent> ]e <Plug>(coc-diagnostic-next)

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

nmap <leader>rn <Plug>(coc-rename)

" Compile and run c program
augroup compile_file
  autocmd!
  autocmd FileType c nnoremap <buffer> <leader>mc :tab te gcc <C-R>=expand("%:t") . ' -o '. expand("%:t:r") . ' && ./' . expand("%:t:r") . " " . CommanLineArgsC()<CR><CR>
  autocmd FileType nroff nnoremap <buffer> <leader>mc :!groff -e % -ms -Tpdf \| zathura -<CR>
  autocmd FileType nroff nnoremap <buffer> <leader>mk :!groff -e % -ms -Tpdf > %:r.pdf<CR>
  autocmd FileType plaintex,tex nnoremap <silent><buffer> <leader>mc :!pdflatex % && zathura %:r.pdf<CR>
augroup end

set hidden
set history=100

set hlsearch                          " higlight search strings
set incsearch                         " incremental search
set ignorecase                        " ignore case when searching
set smartcase                         " except when search includes capital

set showmatch                         " When a bracket is inserted, briefly jump to the matching one.

filetype plugin on

set nocompatible

set path+=**                          " Search downwards in directory

" Tweaks for browsing
let g:netrw_banner=0                  " disable annoying banner
let g:netrw_liststyle=3               " tree view

if has('windows')
  set splitbelow                      " open horizontal splits below current window
endif

if has('vertsplit')
  set splitright                      " open vertical splits to the right of the current window
endif

if has('virtualedit')
  set virtualedit=block               " allow cursor to move where there is no text in visual block mode
endif

set whichwrap=b,h,l,s,<,>,[,],~       " allow <BS>/h/l/<Left>/<Right>/<Space>, ~ to cross line boundaries

if has('wildignore')
  set wildignore+=*.o,*.rej,*.class   " patterns to ignore during file-navigation
  set wildignore+=*/node_modules/*    " ignore node_modules
endif

let g:ctrlp_show_hidden=1             " allow ctrlP to search for dotfiles

if has('wildmenu')
  set wildmenu                        " show options as list when switching buffers etc
endif

set wildmode=longest:full,full        " shell-like autocomplete to unambiguous portion

highlight User2 guibg=#B8BB26 guifg=bg

" Get branch
function! GitBranch()
  let branch=fugitive#head()
  if branch==''
    return ''
  else
    return '('.branch.')'
  endif
endfunction

set statusline=""                     " Reset status bar
set statusline+=%<                    " cut at start
set statusline+=%f\                   " path, end color
set statusline+=\ %m\                 " flags
set statusline+=%{GitBranch()}\       " branch
set statusline+=%=%5l/%L,%4c          " file type
set statusline+=%6P                   " percentage of file

" Highligh 80th column when there is text there
highlight ColorColumn ctermbg=0 guibg=#B8BB26 guifg=black
call matchadd('ColorColumn', '\%81v')

function! CommanLineArgsC()
  let s:text = join(readfile(expand("%")), "\n")
  let s:lst = []
  call substitute(s:text, '\(\/\/ arg: \)\@<=.\{-}\(\n\)\@=', '\=add(s:lst, submatch(0))', 'g')
  return join(s:lst)
endfunction

" Arg wrap
nnoremap <Leader>b :ArgWrap<CR>

let g:argwrap_padded_braces = '{(['

" Close tag

" filenames like *.xml, *.html, *.xhtml, ...
" These are the file extensions where this plugin is enabled.
"
let g:closetag_filenames = '*.html,*.xhtml,*.phtml'

" filenames like *.xml, *.xhtml, ...
" This will make the list of non-closing tags self-closing in the specified files.
"
let g:closetag_xhtml_filenames = '*.xhtml,*.jsx'

" filetypes like xml, html, xhtml, ...
" These are the file types where this plugin is enabled.
"
let g:closetag_filetypes = 'html,xhtml,phtml'

" filetypes like xml, xhtml, ...
" This will make the list of non-closing tags self-closing in the specified files.
"
let g:closetag_xhtml_filetypes = 'xhtml,jsx'

" integer value [0|1]
" This will make the list of non-closing tags case-sensitive (e.g. `<Link>` will be closed while `<link>` won't.)
"
let g:closetag_emptyTags_caseSensitive = 1

" dict
" Disables auto-close if not in a "valid" region (based on filetype)
"
let g:closetag_regions = {
    \ 'typescript.tsx': 'jsxRegion,tsxRegion',
    \ 'javascript.jsx': 'jsxRegion',
    \ }

" Shortcut for closing tags, default is '>'
"
let g:closetag_shortcut = '>'

" Add > at current position without closing the current tag, default is ''
"
let g:closetag_close_shortcut = ',>'

nmap <Leader>ld <Plug>(linediff-operator)
vnoremap <Leader>ld :Linediff<CR>
nnoremap <Leader>lr :LinediffReset<CR>

nnoremap <C-P> :Files<CR>
nnoremap <Leader>hp :Helptags<CR>

nnoremap <C-y> :VS %:p:h/

" create file with subdirectories if needed :E
function s:MKDir(...)
    if         !a:0
           \|| stridx('`+', a:1[0])!=-1
           \|| a:1=~#'\v\\@<![ *?[%#]'
           \|| isdirectory(a:1)
           \|| filereadable(a:1)
           \|| isdirectory(fnamemodify(a:1, ':p:h'))
        return
    endif
    return mkdir(fnamemodify(a:1, ':p:h'), 'p')
endfunction
command! -bang -bar -nargs=? -complete=file E :call s:MKDir(<f-args>) | e<bang> <args>
command! -bang -bar -nargs=? -complete=file VS :call s:MKDir(<f-args>) | vs<bang> <args>

" Clear colors
nnoremap <leader>cr :%s/<C-v>\[\d*;\d*;\d*;\d*;\d*m\\|<C-v>\[0m//g<CR>

iabbrev cl console.log(
iabbrev clj console.log(JSON.stringify(
iabbrev fn () => {
iabbrev prom new Promise((resolve, reject) => {
iabbrev uE useEffect(() => {

nnoremap <leader>rl :set syntax=off<CR>:set syntax=on<CR>

nnoremap <leader>sp :set spell!<CR>:set spelllang=en,el<CR>
nnoremap <leader>db :e .env<CR>/^DB<CR>03l"+y$:DB =@+<CR> 
vnoremap <leader>db <esc>:e .env<CR>/^DB<CR>03l"+y$gv:DB =@+<CR><CR>

nnoremap <leader>tee :w !sudo tee %<CR>

let g:fcitxOn = 0
function! ActivateFcitx()
  if g:fcitxOn
    call system('fcitx-remote -o')
  endif
endfunction

function! DeactivateFcitx()
    if str2nr(system('fcitx-remote')) > 1
      let g:fcitxOn = 1
    else
      let g:fcitxOn = 0
    endif

    call system('fcitx-remote -c')
endfunction

autocmd InsertLeave * call DeactivateFcitx()
autocmd InsertEnter * call ActivateFcitx()
" autocmd CmdLineLeave * call DeactivateFcitx()
" autocmd CmdLineEnter * call DeactivateFcitx()

" nnoremap / :call ActivateFcitx()<CR>/
" onoremap / :call ActivateFcitx()<CR>/
" nnoremap ? :call ActivateFcitx()<CR>?
" noremap ? :call ActivateFcitx()<Cr>?
" nnoremap : :call ActivateFcitx()<CR>:
" noremap : :call ActivateFcitx()<Cr>:

" cnoremap <CR> <CR>:call DeactivateFcitx()<CR>
" cnoremap  :call DeactivateFcitx()<CR>


nnoremap <leader>cd :cd %:h<CR>
nnoremap <leader>ip mV:e ~/vim-config/vimrc<CR>gg2j}OPlug '<esc>"+p0fhd3f/A'<esc>:w<CR>:so $MYVIMRC<CR>:PlugInstall<CR>'V
nnoremap <leader>pi :PlugInstall<CR>

nnoremap <leader>ra :set operatorfunc=Randomize<cr>g@
vnoremap <leader>ra :<c-u>call Randomize(visualmode())<cr>

command! SetMath inoremap <buffer> <C-b> <esc>:s/ / * /g<CR>:nohlsearch<CR>yyA = <C-r>=<C-r>0<CR><CR>

nmap dsf diwds)


function! Randomize(type)
  if a:type ==# 'v'
    let move = "`<v`>"
  elseif a:type ==# 'V'
    let move = "`<V`>"
  elseif a:type ==# ''
    let move = "`<`>"
  elseif a:type ==# 'char'
    let move = "`[v`]"
  elseif a:type ==# 'line'
    let move = "`[V`]"
  endif

  silent exe 'normal!' move.'y'
  let word = @@
  let word = substitute(word,'\([^ ]\)\( *\)\([^ ]\)','\u\1\2\l\3','g')

  let length = strlen(word)
  let i = 0

  while i < luaeval('math.random(1, ' . length .')')
    let i += 1

    let num = luaeval('math.random(1, ' . length . ')')
    let word = substitute(word,'\(.\{' . (num - 1) . '}\)\(.\)\(.*\)','\=submatch(2)==#tolower(submatch(2))?submatch(1).toupper(submatch(2)).submatch(3) : submatch(1).tolower(submatch(2)).submatch(3)','g')
  endwhile

  let @@ = word
  silent exe 'normal!' move.'p'
endfunction


" ------ vim tree----------"
" let g:nvim_tree_side = 'right' "left by default
let g:nvim_tree_width = 40 "30 by default, can be width_in_columns or 'width_in_percent%'
let g:nvim_tree_ignore = [ '.git', 'node_modules', '.cache' ] "empty by default
let g:nvim_tree_gitignore = 1 "0 by default
let g:nvim_tree_auto_open = 0 "0 by default, opens the tree when typing `vim $DIR` or `vim`
let g:nvim_tree_auto_close = 1 "0 by default, closes the tree when it's the last window
let g:nvim_tree_auto_ignore_ft = [ 'startify', 'dashboard' ] "empty by default, don't auto open tree on specific filetypes.
let g:nvim_tree_quit_on_open = 1 "0 by default, closes the tree when you open a file
let g:nvim_tree_follow = 1 "0 by default, this option allows the cursor to be updated when entering a buffer
let g:nvim_tree_indent_markers = 1 "0 by default, this option shows indent markers when folders are open
let g:nvim_tree_hide_dotfiles = 1 "0 by default, this option hides files and folders starting with a dot `.`
let g:nvim_tree_git_hl = 1 "0 by default, will enable file highlight for git attributes (can be used without the icons).
let g:nvim_tree_highlight_opened_files = 1 "0 by default, will enable folder and file icon highlight for opened files/directories.
let g:nvim_tree_root_folder_modifier = ':~' "This is the default. See :help filename-modifiers for more options
let g:nvim_tree_tab_open = 1 "0 by default, will open the tree when entering a new tab and the tree was previously open
let g:nvim_tree_auto_resize = 0 "1 by default, will resize the tree to its saved width when opening a file
let g:nvim_tree_disable_netrw = 0 "1 by default, disables netrw
let g:nvim_tree_hijack_netrw = 0 "1 by default, prevents netrw from automatically opening when opening directories (but lets you keep its other utilities)
let g:nvim_tree_add_trailing = 1 "0 by default, append a trailing slash to folder names
let g:nvim_tree_group_empty = 1 " 0 by default, compact folders that only contain a single folder into one node in the file tree
let g:nvim_tree_lsp_diagnostics = 1 "0 by default, will show lsp diagnostics in the signcolumn. See :help nvim_tree_lsp_diagnostics
let g:nvim_tree_disable_window_picker = 1 "0 by default, will disable the window picker.
let g:nvim_tree_hijack_cursor = 0 "1 by default, when moving cursor in the tree, will position the cursor at the start of the file on the current line
let g:nvim_tree_icon_padding = ' ' "one space by default, used for rendering the space between the icon and the filename. Use with caution, it could break rendering if you set an empty string depending on your font.
let g:nvim_tree_update_cwd = 1 "0 by default, will update the tree cwd when changing nvim's directory (DirChanged event). Behaves strangely with autochdir set.
let g:nvim_tree_window_picker_exclude = {
    \   'filetype': [
    \     'packer',
    \     'qf'
    \   ],
    \   'buftype': [
    \     'terminal'
    \   ]
    \ }
" Dictionary of buffer option names mapped to a list of option values that
" indicates to the window picker that the buffer's window should not be
" selectable.
let g:nvim_tree_special_files = { 'README.md': 1, 'Makefile': 1, 'MAKEFILE': 1 } " List of filenames that gets highlighted with NvimTreeSpecialFile
let g:nvim_tree_show_icons = {
    \ 'git': 1,
    \ 'folders': 0,
    \ 'files': 0,
    \ 'folder_arrows': 0,
    \ }
"If 0, do not show the icons for one of 'git' 'folder' and 'files'
"1 by default, notice that if 'files' is 1, it will only display
"if nvim-web-devicons is installed and on your runtimepath.
"if folder is 1, you can also tell folder_arrows 1 to show small arrows next to the folder icons.
"but this will not work when you set indent_markers (because of UI conflict)

" default will show icon by default if no icon is provided
" default shows no icon by default
let g:nvim_tree_icons = {
    \ 'default': '',
    \ 'symlink': '',
    \ 'git': {
    \   'unstaged': "✗",
    \   'staged': "✓",
    \   'unmerged': "",
    \   'renamed': "➜",
    \   'untracked': "★",
    \   'deleted': "",
    \   'ignored': "◌"
    \   },
    \ 'folder': {
    \   'arrow_open': "",
    \   'arrow_closed': "",
    \   'default': "",
    \   'open': "",
    \   'empty': "",
    \   'empty_open': "",
    \   'symlink': "",
    \   'symlink_open': "",
    \   },
    \   'lsp': {
    \     'hint': "",
    \     'info': "",
    \     'warning': "",
    \     'error': "",
    \   }
    \ }

nnoremap <C-n> :NvimTreeToggle<CR>
nnoremap <leader>r :NvimTreeRefresh<CR>
nnoremap <leader>n :NvimTreeFindFile<CR>
" NvimTreeOpen and NvimTreeClose are also available if you need them

set termguicolors " this variable must be enabled for colors to be applied properly

" a list of groups can be found at `:help nvim_tree_highlight`
highlight NvimTreeFolderIcon guibg=blue
